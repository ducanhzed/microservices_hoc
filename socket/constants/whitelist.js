require('dotenv').config();

const env = process.env
module.exports = Object.keys(env).filter(key_name => key_name.match(/ORIGIN_IP/) ? true : false).map(key_name => process.env[key_name])
