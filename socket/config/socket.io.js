module.exports = {
    cors: {
        origins: [require('../constants/whitelist')],
        methods: ["GET", "POST"],
        credentials: true
    }
}