const EVENTS = require('../constants/socketEvents')
const SocketHandler = require('./handlers')


function Socket(io) {
    // constructor
    this.io = io
    this.ONLINE_USERS = {}
    this.ONLINE_SOCKET_ID = {}
    io
        .use((socket, next) => {
            //check authentication here
            //...

            const { token } = socket.handshake.query

            // if is authenticatedt
            // get the username

            if (!token) return

            const { username = '' } = JSON.parse(token)
            if (!username) return

            this.ONLINE_USERS[username] = this.ONLINE_USERS[username] ? [...this.ONLINE_USERS[username], socket.id] : [socket.id]
            this.ONLINE_SOCKET_ID[socket.id] = username

            console.log(this.ONLINE_USERS)
            console.log(this.ONLINE_SOCKET_ID)
            return next()
            // if not, return null
        })
        .on('connection', socket => {
            console.log(socket.id + ' a user has connected to website')
            const socketHandler = new SocketHandler(socket, { ONLINE_USERS: this.ONLINE_USERS })
            Object.keys(EVENTS).forEach(eventName => socket.on(eventName, socketHandler[eventName] || (() => { })));

            socket.on('disconnect', (data) => {
                const username = this.ONLINE_SOCKET_ID[socket.id]
                console.log(username)

                // remove out of online users
                delete this.ONLINE_SOCKET_ID[socket.id]
                this.ONLINE_USERS[username] = this.ONLINE_USERS[username].filter(e => e !== socket.id)
            })
        })

}

module.exports = Socket