const { IS_ONLINE_CSS, IS_ONLINE_SSC, SEND_MESSAGE_CSS, SEND_MESSAGE_SSC } = require('../constants/socketEvents')

function SocketHandler(socket, { ONLINE_USERS = [] }) {
    //constructor
    this.socket = socket

    // public
    this.IS_ONLINE_CSS = async function (data) {
        const signalCreateNotification = await createNotification(data)
        const message = signalCreateNotification.error ? 'error from server' : 'success message from server'
        sendSignalOnlineToClient({ message })
    }

    this.SEND_MESSAGE_CSS = async (data) => {

    }

    //private
    const sendSignalOnlineToClient = (data) => socket.emit(IS_ONLINE_SSC, data)
    const createNotification = async ({ username = 'default', message = 'default message' }) => {
        // SAVE TO DB
        return await setTimeout(() => {
            return { error: false, message: 'save to db success' }
        }, 500)
    }

    // save message to DB
    const saveMessageToDB = async ({ username = 'default', message = 'default message' }) => {
        // SAVE TO DB
        return await setTimeout(() => {
            return { error: false, message: 'save to db success' }
        }, 500)
    }
}

module.exports = SocketHandler