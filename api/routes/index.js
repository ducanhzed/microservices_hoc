var express = require('express');
var router = express.Router();
const fakeData = require('../constants/fakeData')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.get('/data', (req, res, next) => {
  res.json(fakeData)
})
module.exports = router;
