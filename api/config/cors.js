const whitelist = require('../constants/whitelist')

module.exports = {
    origin: function (origin, callback) {
        console.log(whitelist, origin)
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed by CORS'))
        }
    }
}