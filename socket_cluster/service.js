const MODEL = require('./model')

class Service {
    constructor() { }

    updateOnlineUser = async ({ username = null, socketID = null }) => {
        try {
            if (!username || !socketID)
                return { error: true, msg: 'invalid_params' }
            const exist = await MODEL.findOne({ username })
            let result = {}
            if (exist)
                result = await MODEL.updateOne({ username }, { $push: { socketIDs: socketID } }, { new: true })
            else
                result = await (new MODEL({ username, socketIDs: [socketID] })).save()
            if (result) {
                return { error: false, msg: 'update success' }
            }
        } catch (err) {
            return { error: true, msg: err.message }
        }
    }

    disconnectUpdate = async ({ username = null, socketID = null }) => {
        try {
            if (!username || !socketID)
                return { error: true, msg: 'invalid_params' }

            const exist = await MODEL.findOne({ username })
            let result = {}
            if (exist.socketIDs?.length > 1)
                result = await MODEL.updateOne({ username }, { $pull: { socketIDs: socketID } }, { new: true })
            else
                result = await MODEL.deleteOne({ username })

            if (result) {
                return { error: false, msg: 'update success' }
            }
        } catch (err) {
            return { error: true, msg: err.message }
        }
    }

    getSocketIDsByUsername = async (username = null) => {
        try {
            if (!username)
                return { error: true, msg: 'invalid_params_username' }
            const result = await MODEL.findOne({ username }).select('socketIDs')

            if (result) {
                return { error: false, msg: 'get success', data: result }
            } else {
                return { error: true, msg: 'username is not exist', data: null }
            }
        } catch (err) {
            return { error: true, msg: err.message }
        }
    }

    getList = async () => {
        try {
            const result = await MODEL.find()

            if (result) {
                return { error: false, msg: 'get success', data: result }
            } else {
                return { error: true, msg: 'username is not exist', data: null }
            }
        } catch (err) {
            return { error: true, msg: err.message }
        }
    }

}

module.exports = Service