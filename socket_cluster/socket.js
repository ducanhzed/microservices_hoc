const EVENTS = require('./events')
const SocketHandler = require('./handlers')

function Socket(io) {
    // constructor
    this.io = io;

    io
        .use((socket, next) => {
            //check authentication here
            //...
            const { token } = socket.handshake.query

            // if is authenticatedt
            // get the username

            if (!token) return

            const { username = '' } = JSON.parse(token)
            if (!username) return

            socket.username = username
            io.ONLINE_USERS = io.ONLINE_USERS || {}
            io.ONLINE_SOCKET_ID = io.ONLINE_SOCKET_ID || {}
            io.ONLINE_USERS[username] = io.ONLINE_USERS[username] ? [...io.ONLINE_USERS[username], socket.id] : [socket.id]
            io.ONLINE_SOCKET_ID[socket.id] = username

            return next()
            // if not, return null
        })
        .on('connection', socket => {
            // console.log(socket.id + ' a user has connected to website')

            const socketHandler = new SocketHandler(socket, io)
            Object.keys(EVENTS).forEach(eventName => socket.on(eventName, socketHandler[eventName] || (() => { })));

            socket.on('disconnect', (data) => {
                const username = io.ONLINE_SOCKET_ID[socket.id]
                console.log(username + 'disconnected')

                // remove out of online users
                delete io.ONLINE_SOCKET_ID[socket.id]
                io.ONLINE_USERS[username] = io.ONLINE_USERS[username].filter(e => e !== socket.id)
            })
        })

}

module.exports = Socket