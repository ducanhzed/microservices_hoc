const cluster = require("cluster");
const http = require("http");
const { Server } = require("socket.io");
const redisAdapter = require("socket.io-redis");
const numCPUs = require("os").cpus().length;
const { setupMaster, setupWorker } = require("@socket.io/sticky");
const { RedisClient } = require('redis')
const socket = require('./socket')
const mongoose = require('mongoose');
const figlet = require('figlet')

mongoose.Promise = global.Promise;

if (cluster.isMaster) {
    console.log(`Master ${process.pid} is running`);

    console.log('Server starting at port 5000...')

    const httpServer = http.createServer();

    setupMaster(httpServer, {
        loadBalancingMethod: "least-connection", // either "random", "round-robin" or "least-connection"
    });

    httpServer.listen(5000);
    figlet('JN SOCKET', function (err, data) {
        if (err) {
            console.log('Something went wrong...');
            console.dir(err);
            return;
        }
        console.log(data)
    });

    for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
    }

    cluster.on("exit", (worker) => {
        console.log(`Worker ${worker.process.pid} died`);
        cluster.fork();
    });
} else {
    console.log(`Worker ${process.pid} started`);



    // Connect MongoDB at default port 27017.
    mongoose.connect('mongodb://localhost:27017/test', {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true
    }, (err) => {
        if (!err) {
            console.log('MongoDB Connection Succeeded.')

            const httpServer = http.createServer();
            let io = new Server(httpServer, {
                path: "/general",
                serveClient: false,
                // below are engine.IO options
                pingInterval: 10000,
                pingTimeout: 5000,
                cookie: false,
                cors: {
                    origin: ['http://localhost:3000'],
                    methods: ['GET', 'POST'],
                    credentials: true,
                }
            });

            const pubClient = new RedisClient({ host: 'localhost', port: 6379 });
            const subClient = pubClient.duplicate();

            io.adapter(redisAdapter({ pubClient, subClient }));
            setupWorker(io);

            socket(io)
        } else {
            console.log('Error in DB connection: ' + err)
        }
    });
}