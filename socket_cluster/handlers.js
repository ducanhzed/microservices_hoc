const { IS_ONLINE_CSS, IS_ONLINE_SSC, SEND_MESSAGE_CSS, SEND_MESSAGE_SSC } = require('./events')
const Service = require('./service')
const SERVICE = require('./service')
const socketService = new SERVICE()

function SocketHandler(socket, io) {
    //constructor
    this.socket = socket

    // public
    this.IS_ONLINE_CSS = async function (data) {
        const signalUpdate = await socketService.updateOnlineUser({ username: socket.username, socketID: socket.id })

        if (signalUpdate.error)
            return console.log(signalUpdate.msg)

        socket.broadcast.emit(IS_ONLINE_SSC, { message: 'from server' })
        return await updateListUserOnlineForClients()
    }

    this.SEND_MESSAGE_CSS = async (data) => {
        const signalGetList = await socketService.getSocketIDsByUsername(data.username)
        if (signalGetList.error)
            return console.log(signalGetList.msg)

        // console.log(signalGetList)
        const { data: { socketIDs } } = signalGetList
        emitEventToListSocketIDs({ eventName: 'SEND_MESSAGE_SSC', socketIDs, data: { from: socket.username, message: data.message } })
    }

    this.FIND_SOCKET_ID_SEND = async (data) => {
        // console.log({ username: data })
    }

    this.disconnect = async (reason) => {
        console.log(reason)
        const signalUpdate = await socketService.disconnectUpdate({ username: socket.username, socketID: socket.id })
        console.log({ signalUpdateRemove: signalUpdate })
        return await updateListUserOnlineForClients()
    }

    //private
    const updateListUserOnlineForClients = async () => {
        const signalGetList = await socketService.getList()
        if (signalGetList.error)
            return console.log(signalGetList.msg)
        return io.emit('UPDATE_LIST_USER_ONLINE_SSC', { onlineUsers: signalGetList.data || [] })
    }

    const emitEventToListSocketIDs = ({ eventName = '', data = {}, socketIDs = [''] }) => {
        try {
            if (!socketIDs || !socketIDs.length)
                return console.log('empty_ids_arr')

            socketIDs.forEach(e => {
                console.log(e)
                io.to(e).emit(eventName, data)
            })

            return 'send_success'

        } catch (error) {
            console.log(error)
            return error.message
        }
    }

}

module.exports = SocketHandler