const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSocketModel = new Schema({
    username: { type: String, require: true },
    socketIDs: [{ type: String, trim: true }]
});

module.exports = mongoose.model("socketModel", userSocketModel);