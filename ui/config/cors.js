require('dotenv').config()


const whitelist = require('../constants/whitelist')

module.exports = {
    origin: function (origin, callback) {
        // allow requests with no origin

        if (!origin) return callback(null, true);
        if (whitelist.indexOf(origin) === -1) {
            var message = 'The CORS policy for this origin doesn\'t ' +
                'allow access from the particular origin.';
            return callback(new Error(message), false);
        }
        return callback(null, true);
    },
    credentials: true
}